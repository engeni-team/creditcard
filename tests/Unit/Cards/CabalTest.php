<?php

namespace LVR\CreditCard\Tests\Unit\Cards;

use Illuminate\Support\Collection;
use LVR\CreditCard\Cards\Cabal;

class CabalTest extends BaseCardTests
{
    public $instance = Cabal::class;

    public function validNumbers(): Collection
    {
        return collect([
            '6042650000263755',
        ]);
    }

    public function numbersWithInvalidLength(): Collection
    {
        return collect([
            '60426500002637',
            '604265000026372222',
        ]);
    }

    public function numbersWithInvalidCheckSum(): Collection
    {
        return collect([
            '6042650000263751',
        ]);
    }
}
