<?php

namespace LVR\CreditCard\Tests\Unit\Cards;

use Illuminate\Support\Collection;
use LVR\CreditCard\Cards\Naranja;

class NaranjaTest extends BaseCardTests
{
    public $instance = Naranja::class;

    public function validNumbers(): Collection
    {
        return collect([
            '5895628739614091',
        ]);
    }

    public function numbersWithInvalidLength(): Collection
    {
        return collect([
            '5895628739614091111',
        ]);
    }

    public function numbersWithInvalidCheckSum(): Collection
    {
        return collect();
    }

    public function it_checks_number_checksum()
    {
        $this->assertTrue(true);
    }
}
